import { Http } from '@angular/http';
import {Component} from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';
import {OrcamentoService} from '../../services/orcamento-service';

@Component({
  templateUrl: 'build/pages/modal-produto/modal-produto.html',
})
export class ModalProdutoPage {
  static get parameters() {
    return [[Http],[ViewController], [NavParams]];
  }

  constructor(http, view, params) {
    this.http = http;
    this.view = view;
    this.produto = params.get("produto");
    this.mesa = params.get("mesa");

    this.qtd = 1;
  }

  addProduto(produto){
    this.verifyLastSequence(this.mesa.id, (data) => {
      if(data.SEQUENCIA == "VAZIO"){
        let produtoAdd = {
          'NUMERO': this.mesa.id,
          'SEQUENCIA': 1,
          'CODIGO_PRODUTO': produto.CODIGO,
          'DESCRICAO': produto.DESCRICAO,
          'PRVENDA': produto.PRVENDA,
          'QUANTIDADE': this.qtd,
          'TOTALITEM': produto.PRVENDA*this.qtd,
          'TEXTO_SERVICO': this.observacoes
        }

        this.view.dismiss(produtoAdd);

      }else{
        let produtoAdd = {
          'NUMERO': this.mesa.id,
          'SEQUENCIA': parseInt(data.SEQUENCIA) + 1,
          'CODIGO_PRODUTO': produto.CODIGO,
          'DESCRICAO': produto.DESCRICAO,
          'PRVENDA': produto.PRVENDA,
          'QUANTIDADE': this.qtd,
          'TOTALITEM': produto.PRVENDA*this.qtd,
          'TEXTO_SERVICO': this.observacoes
        }

        this.view.dismiss(produtoAdd);
      }
    });
  }

  addQtd() {
    this.qtd += 1;
  }

  removeQtd() {
    this.qtd -= 1;
  }

  cancel(){
    this.view.dismiss();
  }

  verifyLastSequence(id, successCallback){
    let orcamentoService = new OrcamentoService();

    orcamentoService.verifyLastSequence(id, this.http, (data) => {
      successCallback(data);
    });
  }
}
