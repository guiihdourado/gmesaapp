import { Http } from '@angular/http';
import {Component} from '@angular/core';
import { Modal, NavController, NavParams, Toast, Events} from 'ionic-angular';
import {ProdutoService} from '../../services/produto-service';
import {ModalProdutoPage} from '../modal-produto/modal-produto';
import {OrcamentoService} from '../../services/orcamento-service';

@Component({
  templateUrl: 'build/pages/produto/produto.html',
})

export class ProdutoPage {
  static get parameters() {
    return [[Http], [NavController], [NavParams], [Events]];
  }

  constructor(http, nav, params, events) {
    this.nav = nav;
    this.http = http;
    this.events = events;

    this.grupo = params.get("grupo");
    this.mesa = params.get("mesa");

    let produtoService = new ProdutoService();

    produtoService.getProdutos(this.grupo, this.http, (data) => {
      let produtosAlt = [];
      this.produtos = data;
      this.produtos.forEach(function(value, key){
        let object = {
          CODIGO: value.CODIGO,
          DESCRICAO: value.DESCRICAO,
          PRVENDA: parseFloat(value.PRVENDA)
        }

        produtosAlt.push(object);
      });

      this.produtosAlt = produtosAlt;
    });
  }

  addPushProduto(p) {
    let modal = Modal.create(ModalProdutoPage, {produto: p, mesa: this.mesa});

    modal.onDismiss((res) => {
      if(res){
        this.createOrcamento(JSON.stringify(res), (data) => {
          if(data.cadastrado = 'true'){
            let toast = Toast.create({
              message: 'Produto Adicionado com Sucesso.',
              duration: 3000,
              cssClass: 'center'
            });

            this.events.publish("produtos:updated", "Atualizando Produtos");

            this.nav.present(toast);
          }else{
            let toast = Toast.create({
              message: 'O Produto não pode ser adicionado, tente novamente.',
              duration: 3000,
              cssClass: 'center'
            });

            this.nav.present(toast);
          }
        });
      }
    });

    this.nav.present(modal);
  }

  addProduto(p) {
    console.log(p);
  }

  createOrcamento(attributes, successCallback){
    let orcamentoService = new OrcamentoService();

    orcamentoService.createOrcamento(attributes, this.http, (data) => {
      successCallback(data);
    });
  }
}
