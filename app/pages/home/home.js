import { Http } from '@angular/http';
import {Component} from '@angular/core';
import {Platform, NavController, ActionSheet, Toast, Alert} from 'ionic-angular';
import {Param2Service} from '../../services/param2-service';
import {FecorcaService} from '../../services/fecorca-service';
import {OrcamentoService} from '../../services/orcamento-service';
import {VendedorService} from '../../services/vendedor-service';
import {DataUtil} from '../../util/data-util';
import {TabsPage} from '../tabs/tabs';
import 'rxjs/add/operator/map';

@Component({
  templateUrl: 'build/pages/home/home.html'
})

export class HomePage {

  static get parameters(){
    return [[Http], [NavController], [Platform]];
  }

  constructor(http, nav, platform) {
    this.nav = nav;
    this.http = http;
    this.platform = platform;

    this._reloadMesas();
  }

  _reloadMesas() {
    let param2Service = new Param2Service();
    let vendedorService = new VendedorService();

    param2Service.getQtdMesas(this.http, (res) => {
      let qtdMesas = res.numerodemesas;

      this.getMesas(qtdMesas, (lista) => {
        this.mesas = lista;
      });
    });

    vendedorService.getVendedores(this.http, (data) => {
      this.vendedores = data;
    });
  }

  getMesas(qtdMesas, successCallback) {
    var mesas = [];
  	for(var i = 1; i <= qtdMesas; i++) {
  		this.getFlagMesa(i, (res) => {
        mesas.push(res);

      });
      successCallback(mesas);
  	}
  }

  getFlagMesa(id, successCallback) {
    let fecorcaService = new FecorcaService();

    fecorcaService.getFecorca(id, this.http, (data) => {
      let mesaPush;
      if(data.NUMERO == "vazio"){
        mesaPush = {id: id, nome: "Mesa "+(id), flag: "disponivel"};
      }else if(data.MESAABERTA == "SIM" && (data.MESAFECHA == null || data.MESAFECHA == "NAO")){
        mesaPush = {id: id, nome: "Mesa "+(id), flag: "aberta"};
      }else if(data.MESAABERTA == "SIM" && data.MESAFECHA == "SIM"){
        mesaPush = {id: id, nome: "Mesa "+(id), flag: "fechamento"};
      }
      successCallback(mesaPush);
    });
  }

  getFlagColor(mesa) {
    switch (mesa.flag){
      case 'disponivel':
        return "disponivel";
        break;
      case 'aberta':
        return "aberta";
        break;
      case 'fechamento':
        return "fechamento";
        break;
    }
  }

  presentActionSheet(item) {
    let actionSheet = ActionSheet.create({
      title: item.nome,
      buttons: [
        {
          text: 'Abrir Mesa/Adicionar Produtos',
          icon: !this.platform.is('ios') ? 'open' : null,
          handler: () => {
            let prompt = Alert.create({
              title: 'Abrir Mesa/Adicionar Produtos',
              message: "Insira seu código de identificação",
              inputs: [
                {
                  name: 'codigoidentificacao',
                  placeholder: 'Código de Identificação',
                  type: 'number'
                },
              ],
              buttons: [
                {
                  text: 'Cancelar',
                  handler: data => {
                    this._reloadMesas();
                  }
                },
                {
                  text: 'Ok',
                  handler: data => {
                    let vendedor = this.verifyVendedor(data.codigoidentificacao);
                    if(vendedor == null){
                      let toast = Toast.create({
                        message: 'Código de identificação inválido.',
                        duration: 3000,
                        cssClass: 'center'
                      });

                      this._reloadMesas();
                      this.nav.present(toast);
                    }else{
                      if(this.isDisponivel(item)){
                        let dataUtil = new DataUtil();
                        let novaData = new Date().toLocaleDateString();

                        let replaceData = dataUtil.parseData(novaData);
                        let hora = new Date().toLocaleTimeString();

                        let attributes = {
                          'NUMERO': item.id,
                          'DATA': replaceData,
                          'HORA': hora,
                          'VENDEDOR': vendedor.CODIGO,
                          'PER_ACRESCIMO': 10.0
                        }

                        this.createFecorca(JSON.stringify(attributes), (data) => {
                          let toast = Toast.create({
                            message: item.nome+' aberta com sucesso.',
                            duration: 3000,
                            cssClass: 'center'
                          });

                          this._reloadMesas();
                          this.nav.present(toast);
                        });
                      }else if (this.isFechamento(item)) {
                        let toast = Toast.create({
                          message: item.nome + ' em modo de fechamento.',
                          duration: 3000,
                          cssClass: 'center'
                        });

                        this.nav.present(toast);
                      }else{
                        this.nav.push(TabsPage, {parametro: item});
                      }
                    }
                  }
                }
              ]
            });

            this.nav.present(prompt);
          }
        },{
          text: 'Trocar Mesa',
          icon: !this.platform.is('ios') ? 'swap' : null,
          handler: () => {
            let prompt = Alert.create({
              title: 'Trocar Mesa',
              message: "Insira seu código de identificação",
              inputs: [
                {
                  name: 'codigoidentificacao',
                  placeholder: 'Código de Identificação',
                  type: 'number'
                },
              ],
              buttons: [
                {
                  text: 'Cancelar',
                  handler: data => {
                    this._reloadMesas();
                  }
                },
                {
                  text: 'Ok',
                  handler: data => {
                    let vendedor = this.verifyVendedor(data.codigoidentificacao);
                    if(vendedor == null){
                      let toast = Toast.create({
                        message: 'Código de identificação inválido.',
                        duration: 3000,
                        cssClass: 'center'
                      });

                      this._reloadMesas();
                      this.nav.present(toast);
                    }else{
                      if(this.isFechamento(item)){
                        let toast = Toast.create({
                          message: item.nome + ' em modo de fechamento.',
                          duration: 3000,
                          cssClass: 'center'
                        });

                        this._reloadMesas();
                        this.nav.present(toast);
                      }else{
                        let prompt = Alert.create({
                          title: 'Trocar Mesa',
                          message: "Digite o nº da mesa que receberá os itens.",
                          inputs: [
                            {
                              name: 'mesa',
                              placeholder: 'N° da mesa',
                              type: 'tel'
                            },
                          ],
                          buttons: [
                            {
                              text: 'Cancelar',
                              handler: data => {
                                this._reloadMesas();
                              }
                            },
                            {
                              text: 'Trocar',
                              handler: data => {
                                if(data.mesa > this.mesas.length){
                                  let toast = Toast.create({
                                    message: 'Essa mesa não existe.',
                                    duration: 3000,
                                    cssClass: 'center'
                                  });

                                  this._reloadMesas();
                                  this.nav.present(toast);
                                }else{
                                  let fecorcaService = new FecorcaService();
                                  let orcamentoService = new OrcamentoService();

                                  fecorcaService.getFecorca(data.mesa, this.http, (res) => {
                                    if(res.NUMERO == 'vazio'){
                                      let confirmarTroca = Alert.create({
                                        message: 'Deseja realmente enviar os items da '+item.nome+' para a Mesa '+data.mesa+' ?',
                                        buttons: [
                                          {
                                            text: 'NÃO',
                                            handler: () => {
                                              this._reloadMesas();
                                            }
                                          },
                                          {
                                            text: 'SIM',
                                            handler: () => {
                                              let attributes = {
                                                'NUMERO': item.id,
                                                'NUMEROALT': data.mesa
                                              }

                                              this.updateSwitchTableFecorca(JSON.stringify(attributes), (data) => {
                                                this.updateProductOrcamento(JSON.stringify(attributes), (data) => {
                                                  let toast = Toast.create({
                                                    message: 'Atualizado com sucesso.',
                                                    duration: 3000,
                                                    cssClass: 'center'
                                                  });

                                                  this._reloadMesas();
                                                  this.nav.present(toast);
                                                });
                                              });  
                                            }
                                          }
                                        ]
                                      });
                                      this.nav.present(confirmarTroca);
                                    }else{
                                      console.log("NUMERO NÃO ESTA VAZIO");
                                    }
                                  });
                                }
                              }
                            }
                          ]
                        });

                        this.nav.present(prompt);
                      }
                    }
                  }
                }
              ]
            });

            this.nav.present(prompt);
          }
        },{
          text: 'Juntar Mesa',
          icon: !this.platform.is('ios') ? 'people' : null,
          handler: () => {
            console.log('Juntar Mesa ' + item.nome);
          }
        },{
          text: 'Cancelar Mesa',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            let prompt = Alert.create({
              title: 'Cancelar Mesa',
              message: "Insira seu código de identificação",
              inputs: [
                {
                  name: 'codigoidentificacao',
                  placeholder: 'Código de Identificação',
                  type: 'number'
                },
              ],
              buttons: [
                {
                  text: 'Cancelar',
                  handler: data => {
                    this._reloadMesas();
                  }
                },
                {
                  text: 'Ok',
                  handler: data => {
                    let vendedor = this.verifyVendedor(data.codigoidentificacao);
                    if(vendedor == null){
                      let toast = Toast.create({
                        message: 'Código de identificação inválido.',
                        duration: 3000,
                        cssClass: 'center'
                      });

                      this._reloadMesas();
                      this.nav.present(toast);
                    }else{
                      this.getOrcamento(item.id, (data) => {
                        if(data.NUMERO == 'vazio'){
                          let toast = Toast.create({
                            message: 'Não é possível cancelar pois não existe produtos lançados na mesa.',
                            duration: 3000,
                            cssClass: 'center'
                          });
                          this.nav.present(toast);
                        }else{
                          this.deleteOrcamentoWithFecorca(item.id, (data) => {
                            this.deleteFecorca(item.id, (data) => {
                              let toast = Toast.create({
                                message: 'Mesa cancelada com sucesso.',
                                duration: 3000,
                                cssClass: 'center'
                              });

                              this._reloadMesas();
                              this.nav.present(toast);
                            });
                          });
                        }
                      });
                    }
                  }
                }
              ]
            });

            this.nav.present(prompt);

          }
        },{
          text: 'Fechar Mesa',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            let prompt = Alert.create({
              title: 'Fechar Mesa',
              message: "Insira seu código de identificação",
              inputs: [
                {
                  name: 'codigoidentificacao',
                  placeholder: 'Código de Identificação',
                  type: 'number'
                },
              ],
              buttons: [
                {
                  text: 'Cancelar',
                  handler: data => {
                    this._reloadMesas();
                  }
                },
                {
                  text: 'Ok',
                  handler: data => {
                    let vendedor = this.verifyVendedor(data.codigoidentificacao);
                    if(vendedor == null){
                      let toast = Toast.create({
                        message: 'Código de identificação inválido.',
                        duration: 3000,
                        cssClass: 'center'
                      });

                      this._reloadMesas();
                      this.nav.present(toast);
                    }else{
                      let attributes = {
                        "NUMERO": item.id,
                        "MESAFECHA": "SIM"
                      }

                      if(this.isDisponivel(item)){
                        let toast = Toast.create({
                          message: item.nome+' não foi aberta.',
                          duration: 3000,
                          cssClass: 'center'
                        });

                        this._reloadMesas();
                        this.nav.present(toast);
                      }else{
                        if(item.flag == "fechamento"){
                          let toast = Toast.create({
                            message: item.nome+' já em fechamento.',
                            duration: 3000,
                            cssClass: 'center'
                          });

                          this._reloadMesas();
                          this.nav.present(toast);
                        }else{
                          this.setFecorcaFechamento(JSON.stringify(attributes), (data) => {
                            let toast = Toast.create({
                              message: item.nome+' em fechamento.',
                              duration: 3000,
                              cssClass: 'center'
                            });

                            this._reloadMesas();
                            this.nav.present(toast);
                          });
                        }
                      }
                    }
                  }
                }
              ]
            });

            this.nav.present(prompt);

          }
        },{
          text: 'Libera Mesa',
          icon: !this.platform.is('ios') ? 'undo' : null,
          handler: () => {

            let prompt = Alert.create({
              title: 'Liberar Mesa',
              message: "Insira seu código de identificação",
              inputs: [
                {
                  name: 'codigoidentificacao',
                  placeholder: 'Código de Identificação',
                  type: 'number'
                },
              ],
              buttons: [
                {
                  text: 'Cancelar',
                  handler: data => {
                    this._reloadMesas();
                  }
                },
                {
                  text: 'Ok',
                  handler: data => {
                    let vendedor = this.verifyVendedor(data.codigoidentificacao);
                    if(vendedor == null){
                      let toast = Toast.create({
                        message: 'Código de identificação inválido.',
                        duration: 3000,
                        cssClass: 'center'
                      });

                      this._reloadMesas();
                      this.nav.present(toast);
                    }else{
                      let attributes = {
                        "NUMERO": item.id,
                        "MESAFECHA": "NAO"
                      }

                      if(this.isDisponivel(item)){
                        let toast = Toast.create({
                          message: item.nome+' não foi aberta.',
                          duration: 3000,
                          cssClass: 'center'
                        });

                        this._reloadMesas();
                        this.nav.present(toast);
                      }else{
                        if(item.flag != "fechamento"){
                          let toast = Toast.create({
                            message: item.nome+' não está em modo fechamento.',
                            duration: 3000,
                            cssClass: 'center'
                          });

                          this._reloadMesas();
                          this.nav.present(toast);
                        }else{
                          this.setFecorcaFechamento(JSON.stringify(attributes), (data) => {
                            let toast = Toast.create({
                              message: item.nome+' liberada com sucesso.',
                              duration: 3000,
                              cssClass: 'center'
                            });

                            this._reloadMesas();
                            this.nav.present(toast);
                          });
                        }
                      }
                    }
                  }
                }
              ]
            });

            this.nav.present(prompt);
          }
        },{
          text: 'Cancelar',
          handler: () => {
            this._reloadMesas();
          }
        }
      ]
    });
    this.nav.present(actionSheet);
  }

  getOrcamento(id, successCallback){
    let orcamentoService = new OrcamentoService();

    orcamentoService.getOrcamento(id, this.http, (data) => {
      successCallback(data);
    });
  }

  deleteOrcamentoWithFecorca(id, successCallback){
    let orcamentoService = new OrcamentoService();

    orcamentoService.deleteOrcamentoWithFecorca(id, this.http, (data) => {
      successCallback(data);
    });
  }

  deleteFecorca(id, successCallback){
    let fecorcaService = new FecorcaService();

    fecorcaService.deleteFecorca(id, this.http, (data) => {
      successCallback(data);
    });
  }

  setFecorcaFechamento(attributes, successCallback){
    let fecorcaService = new FecorcaService();
    fecorcaService.setFecorcaFechamento(attributes, this.http, (data) => {
      successCallback(data);
    });
  }

  isDisponivel(item){
    return item.flag == "disponivel" ? 1 : 0;
  }

  isAberta(item){
    return item.flag == "aberta" ? 1 : 0;
  }

  isFechamento(item){
    return item.flag == "fechamento" ? 1 : 0;
  }

  verifyVendedor(codigoIdentificacao){
    let vendedorSelecionado = null;

    this.vendedores.forEach(function(value, key){
      if(value.CODIGOIDENTIFICACAO == codigoIdentificacao){
        vendedorSelecionado = value;
      }
    });

    return vendedorSelecionado;
  }

  createFecorca(attributes, successCallback){
    let fecorcaService = new FecorcaService();
    fecorcaService.createFecorca(attributes, this.http, (data) => {
      successCallback(data);
    });
  }

  updateSwitchTableFecorca(attributes, successCallback){
     let fecorcaService = new FecorcaService();
     console.log(this.http);
     fecorcaService.updateSwitchTableFecorca(attributes, this.http, (data) => {
      successCallback(data);
     });
  }

  updateProductOrcamento(attributes, successCallback){
    let orcamentoService = new OrcamentoService();
    orcamentoService.updateProductOrcamento(attributes, this.http, (data) => {
      successCallback(data);
    });
  }

}