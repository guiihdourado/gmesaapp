import {Http} from '@angular/http';
import {Component} from '@angular/core';
import {NavController, NavParams, Toast, Events, Alert} from 'ionic-angular';
import {MesaPage} from '../mesa/mesa';
import {GrupoPage} from '../grupo/grupo';
import {OrcamentoService} from '../../services/orcamento-service';

@Component({
  templateUrl: 'build/pages/tabs/tabs.html',
})

export class TabsPage {
  static get parameters() {
    return [[Http], [NavController], [NavParams], [Events]];
  }

  constructor(http, nav, params, events) {
    this.http = http;
    this.nav = nav;

    this.item = params.get("parametro");

    this._Orcamentos((data) => {
      if(data != 0){
        this.produtosOrc = data;
        this.subTotal = this._subTotal(this.produtosOrc);
      }else{
        this.orcamentoVazio = 0;
      }
    });

    events.subscribe("produtos:updated", (data) => {
      this._Orcamentos((res) => {
        console.log(res);
        if(res != 0){
          this.produtosOrc = res;
          this.subTotal = this._subTotal(this.produtosOrc);
        }else{
          this.orcamentoVazio = 0;
        }
      });
    });
  }

  _subTotal(produtos) {
    let subTotal = 0;
    produtos.forEach(function(value){
      subTotal += value.PRVENDA * value.QUANTIDADE;
    })

    return subTotal;
  }

  _Orcamentos(successCallback){
    let orcamentoService = new OrcamentoService();

    orcamentoService.getOrcamento(this.item.id, this.http, (data) => {
      if(data.NUMERO == 'vazio'){
        this.produtos = 0;

        successCallback(this.produtos);
      }else{
        let produtosOrc = [];
        this.produtos = data;
        this.produtos.forEach(function(value, key){
          let object = {
            NUMERO: value.NUMERO,
            SEQUENCIA: value.SEQUENCIA,
            CODIGO_PRODUTO: value.CODIGO_PRODUTO,
            DESCRICAO: value.DESCRICAO,
            QUANTIDADE: value.QUANTIDADE,
            TOTALITEM: parseFloat(value.TOTALITEM),
            TEXTO_SERVICO: value.TEXTO_SERVICO,
            PRVENDA: parseFloat(value.PRVENDA)
          }

          produtosOrc.push(object);
        });

        successCallback(produtosOrc);
      }
    });
  }

  add() {
    this.nav.push(GrupoPage, {parametro: this.item});
  }

  visualizarObservacao(produto) {

    if(produto.TEXTO_SERVICO != null && produto.TEXTO_SERVICO != ""){
      let toast = Toast.create({
        message: produto.DESCRICAO+': '+produto.TEXTO_SERVICO,
        showCloseButton: true,
        closeButtonText: "Fechar"
      });

      this.nav.present(toast);
    }else{
      let toast = Toast.create({
        message: 'Sem observações.',
        showCloseButton: true,
        closeButtonText: "Fechar"
      });

      this.nav.present(toast);
    }
  }

  excluirProduto(produto){
    let confirm = Alert.create({
      message: 'Deseja realmente excluir '+ produto.DESCRICAO +' ?',
      buttons: [
        {
          text: 'NÃO',
          handler: () => {
            this._Orcamentos((data) => {
              if(data != 0){
                this.produtosOrc = data;
                this.subTotal = this._subTotal(this.produtosOrc);
              }else{
                this.orcamentoVazio = 0;
              }
            });
          }
        },
        {
          text: 'SIM',
          handler: () => {
            let orcamentoService = new OrcamentoService();

            let attributes = {
              'NUMERO': produto.NUMERO,
              'SEQUENCIA': produto.SEQUENCIA,
              'CODIGO_PRODUTO': produto.CODIGO_PRODUTO
            };

            orcamentoService.deleteProductOrcamento(JSON.stringify(attributes), this.http, (data) => {
              let toast = Toast.create({
                message: produto.DESCRICAO + ' excluído com sucesso.',
                duration: 3000,
                cssClass: 'center'
              });

              this._Orcamentos((data) => {
                if(data != 0){
                  this.produtosOrc = data;
                  this.subTotal = this._subTotal(this.produtosOrc);
                }else{
                  this.orcamentoVazio = 0;
                }
              });

              this.nav.present(toast);
            });
          }
        }
      ]
    });

    this.nav.present(confirm);
  }
}
