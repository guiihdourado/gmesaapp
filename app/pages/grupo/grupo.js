import { Http } from '@angular/http';
import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {GrupoService} from '../../services/grupo-service';
import {ProdutoPage} from '../produto/produto';

@Component({
  templateUrl: 'build/pages/grupo/grupo.html',
})

export class GrupoPage {
  static get parameters() {
    return [[Http], [NavController], [NavParams]];
  }

  constructor(http, nav, params) {
    this.nav = nav;
    this.http = http;

    this.item = params.get("parametro");

    let grupoService = new GrupoService();

    grupoService.getGrupos(this.http, (data) => {
      this.grupos = data;
    });
  }

  loadProdutos(grupo) {
    this.nav.push(ProdutoPage, {grupo: grupo, mesa: this.item});
  }
}
