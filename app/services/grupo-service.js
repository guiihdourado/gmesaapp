import { Headers } from '@angular/http';

export class GrupoService {

  constructor() {
    this.urlApi = "http://192.168.25.61:8000/api/v1/";
  }

  getGrupos(http, successCallback){
    http.get(this.urlApi+'grupos').map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }
}
