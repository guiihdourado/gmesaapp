import { Headers } from '@angular/http';

export class FecorcaService {

  constructor() {
		this.urlApi = "http://192.168.25.61:8000/api/v1/";
	}

  getFecorca(id, http, successCallback) {
    http.get(this.urlApi+'fecorca/'+id).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  deleteFecorca(id, http, successCallback) {
    http.delete(this.urlApi+'fecorca/'+id).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  setFecorcaFechamento(attributes, http, successCallback){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    http.post(this.urlApi+'fecorca/update', attributes, {headers: headers}).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  createFecorca(attributes, http, successCallback){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    http.post(this.urlApi+'fecorca/create', attributes, {headers: headers}).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  updateSwitchTableFecorca(attributes, http, successCallback){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    http.post(this.urlApi+'fecorca/updateswitch', attributes, {headers: headers}).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

}
