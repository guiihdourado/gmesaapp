export class VendedorService {

  constructor() {
    this.urlApi = "http://192.168.25.61:8000/api/v1/";
  }

  getVendedores(http, successCallback){
    http.get(this.urlApi+'vendedor').map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }
}
