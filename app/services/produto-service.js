import { Headers } from '@angular/http';

export class ProdutoService {

  constructor() {
    this.urlApi = "http://192.168.25.61:8000/api/v1/";
  }

  getProdutos(id, http, successCallback){
    http.get(this.urlApi+'produtos/'+id).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }
}
