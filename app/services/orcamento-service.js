import { Headers } from '@angular/http';

export class OrcamentoService {

  constructor() {
		this.urlApi = "http://192.168.25.61:8000/api/v1/";
	}

  getOrcamento(id, http, successCallback) {
    http.get(this.urlApi+'orcamento/'+id).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  deleteOrcamentoWithFecorca(id, http, successCallback) {
    http.post(this.urlApi+'orcamento/fecorca/'+id).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  verifyLastSequence(id, http, successCallback){
    http.post(this.urlApi+'orcamento/verifysequence/'+id).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  createOrcamento(attributes, http, successCallback){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    http.post(this.urlApi+'orcamento/create', attributes, {headers: headers}).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  deleteProductOrcamento(attributes, http, successCallback){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    http.post(this.urlApi+'orcamento/destroy', attributes, {headers: headers}).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

  updateProductOrcamento(attributes, http, successCallback){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');

    http.post(this.urlApi+'orcamento/update', attributes, {headers: headers}).map(res => res.json()).subscribe(data => {
      successCallback(data);
    });
  }

}
