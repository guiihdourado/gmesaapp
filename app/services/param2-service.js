export class Param2Service {

	constructor() {
		this.urlApi = "http://192.168.25.61:8000/api/v1/";
	}

	getQtdMesas(http, successCallback) {
		http.get(this.urlApi+'qtdmesas').map(res => res.json()).subscribe(data => {
        successCallback(data);
    });
	}

}
